# CURRICULUM VITAEA
***
## IDENTIFICATION
### **Samar Wertani**  
Née le 20/08/1993  
Tél 0755044800  
E-mail: samar.wertani@gmail.com   
Nationalité: Tunisienne
***
## FORMATION ET DIPLOME
### **_Juin 2016_**    Institut Supérieur de gestion    
Licence aplliqué en ingénierie économie et financiére                
Mention: bien 
Session: principale  
### **_Juin 2012_**  Lycée secondaires Bachir Nabhéni
Baccalauréat en économie et gestion
Mention: bien
Session: principale
### Institut Bourguiba des langues vivantes
Spécialité: Anglais
Mention: trés bien
Session: Principale                  
***
## EXPERIENCES PROFESSIONNELLES
### **_DepuisDécembre 2016_** Chargée clientéle à l'assurance Filiassur
### **_Février-Mai 2016_** Stage de projet de fin d'étude au sein de la Banque Internationale Arabe de Tunis   
Département Back office    
Effectuer des opérations de changes     
Assurer la gestion administratives et le suivi des comptes
### **_Janvier-Décembre 2014_** Assistante commerciale de l'Agence immobiliére Jlidi    
Traiter les demanades ces clients   
Prospecter des nouveaux clients   
Détecter les besoins des clients
***
## CONNAISSANCES LINGUISTIQUES   
***Français*** :Lu,écrit et parlé   
***Anglais***  :Lu et écrit   
***Arabe***    :Lu,écrit et parlé
***
## ACTIVITES EXTRA PROFESSIONNELES
***Loisirs*** :Voyager    
***Sport*** :Volley-ball
